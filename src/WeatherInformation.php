<?php

namespace Drupal\weather;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

use Drupal\weather\Exception\CurrencyNotAvailableException;

/**
 * Gets weather information.
 */
class WeatherInformation extends DefaultPluginManager implements WeatherInformationInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs the ImageToolkitManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct('Plugin/WeatherInformation', $namespaces, $module_handler, WeatherInformationPluginInterface::class);

    $this->setCacheBackend($cache_backend, 'weather_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * Gets the active weather plugin.
   *
   * @return string|bool
   *   ID of the active weather information plugin.
   */
  protected function getActiveWeatherPluginId() {
    $plugin_id = $this->configFactory->get('weather.settings')->get('active_weather_plugin');
    return $plugin_id;
  }

  /**
   * Returns the active Plugin for weather information.
   *
   * @return \Drupal\weather\WeatherInformationInterface
   *   The active weather information plugin.
   */
  protected function getActiveWeatherPlugin() {
    if ($plugin_id = $this->getActiveWeatherPluginId()) {
      return $this->createInstance($plugin_id);
    }
    else {
      throw new \Exception('No active weather information plugin set.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentWeatherByPostcode(string $postcode, string $country_code) {
    $plugin = $this->getActiveWeatherPlugin();
    return $plugin->getCurrentWeatherByPostcode();
  }

}
