<?php

namespace Drupal\weather\Exception;

/**
 * Thrown when unable to get information by Yahoo weather services.
 */
class YahooWeatherInformationException extends \RuntimeException {}
