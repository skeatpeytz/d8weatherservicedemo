<?php

namespace Drupal\weather;

/**
 * Defines the interface for weather information plugins.
 */
interface WeatherInformationPluginInterface {

  /**
   * Gets current weather information for a given postcode.
   * 
   * @param string $postcode
   *   The module handler.
   * @param string $country_code
   *   Country code in ISO 3166 format.
   * 
   * @return string
   *   The description of the current weather.
   */
  public function getCurrentWeatherByPostcode(string $postcode, string $country_code);

}
