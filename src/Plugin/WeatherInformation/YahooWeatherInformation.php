<?php

namespace Drupal\weather\Plugin\WeatherInformation;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Utility\Error;
use Drupal\weather\WeatherInformationPluginInterface;
use Drupal\weather\Exception\YahooWeatherInformationException;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Gets weather information from Yahoo weather service.
 *
 * @Plugin(
 *   id = "yahoo",
 *   label = @Translation("Yahoo weather information"),
 * )
 */
class YahooWeatherInformation implements WeatherInformationPluginInterface {

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * API BASE URL incl trailing slash.
   *
   * @var string
   */
  const API_URL = 'https://api.yahoo.com/';

  /**
   * Creates an YahooWeatherInformation instance.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config factory.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(Config $config, Client $http_client, LoggerInterface $logger) {
    $this->config = $config->get('weather.yahoo_weather_settings');
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')->get('ij_currencies.settings'),
      $container->get('http_client'),
      $container->get('logger.factory')->get('ij_currencies')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentWeatherByPostcode(string $postcode, string $country_code) {
    $request_params = ['zip' => $postcode . ',' . $country_code];
    $responce = $this->request($request_params);

    return $responce['description'];
  }

  /**
   * Makes request to yahoo weather service.
   */
  private function request(array $params) {
    $params += [
      'app_id' => $this->config->get('api_key'),
    ];

    try {
      $response = $this->httpClient->get(self::API_URL, ['query' => $params]);
      $response_code = $response->getStatusCode();
      if ($response_code === 200) {
        return Json::decode($response->getBody());
      } else {
        throw new \Exception($response_code . '|' . $response->getBody()->getContents());
      }
    } catch (\Throwable $e) {
      $this->logger->error('Yahoo weather service responds: %description',
        [
          '%description' => Error::renderExceptionSafe($e),
        ]
      );
      // Rethrow.
      throw new YahooWeatherInformationException('Unable to get information from yahoo weather service.');
    }
  }

}
